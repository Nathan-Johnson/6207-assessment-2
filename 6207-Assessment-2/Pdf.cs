﻿using System;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Windows;
using System.Windows.Media;
using System.IO.Packaging;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Xps;

namespace _6207_Assessment_2
{
    class Pdf
    {
        public void convertToXps(Uri path, FrameworkElement surface)
        {
            if (path == null) return;

            // Save current canvas transorm
            Transform transform = surface.LayoutTransform;

            // Temporarily reset the layout transform before saving
            surface.LayoutTransform = null;

            // Get the size of the canvas
            Size size = new Size(surface.Width, surface.Height);

            // Measure and arrange elements
            size.Width = 0;
            size.Height = 0;
            //surface.Measure(size);
            surface.Arrange(new Rect(size));

            // Open new package
            Package package = Package.Open(path.LocalPath, FileMode.Create);

            // Create new xps document based on the package opened
            XpsDocument doc = new XpsDocument(package);

            // Create an instance of XpsDocumentWriter for the document
            XpsDocumentWriter writer = XpsDocument.CreateXpsDocumentWriter(doc);

            // Write the canvas (as Visual) to the document
            writer.Write(surface);

            // Close document
            doc.Close();

            // Close package
            package.Close();

            // Restore previously saved layout
            surface.LayoutTransform = transform;
        }


        public void convertToPdf(string xpsPath, string pdfPath)
        {
            //based on filepaths that will be part of the variable section + .xps or .pdf file extension
            using (PdfSharp.Xps.XpsModel.XpsDocument pdfXpsDoc = PdfSharp.Xps.XpsModel.XpsDocument.Open(xpsPath))
            {
                XpsConverter.Convert(pdfXpsDoc, pdfPath, 0);
                //opens the pdf after creation
                Process.Start(pdfPath);
            }
            //deletes the xps file used after conversion.
            File.Delete(xpsPath);
        }
    }
}


    
