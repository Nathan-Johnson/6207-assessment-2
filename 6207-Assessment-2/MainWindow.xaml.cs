﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _6207_Assessment_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        /*
        public ImageBrush imageBinding
        {

            get
            {
                
                string imagePath = @"\..\..\Images\Main-window-background.jpg";

                Uri path = new Uri(imagePath, UriKind.Relative);
                ImageBrush brush = new ImageBrush();
                brush.ImageSource = new BitmapImage(new Uri(imagePath, UriKind.Relative));

                return brush;
            }
        }
        */
        public void Button_Click(object sender, RoutedEventArgs e)
        {
            string[] nouns = new string[9];
            string[] verbs = new string[9];

            nouns[1] = noun1.Text;
            nouns[2] = noun2.Text;
            nouns[3] = noun3.Text;
            nouns[4] = noun4.Text;
            nouns[5] = noun5.Text;
            nouns[6] = noun6.Text;
            nouns[7] = noun7.Text;
            nouns[8] = noun8.Text;

            verbs[1] = verb1.Text;
            verbs[2] = verb2.Text;
            verbs[3] = verb3.Text;
            verbs[4] = verb4.Text;
            verbs[5] = verb5.Text;
            verbs[6] = verb6.Text;
            verbs[7] = verb7.Text;
            verbs[8] = verb8.Text;

          
            
            if (selector.SelectedValue == comedy)
            {
                Console.WriteLine("comedy selected");
                ComedyWindow comedyPage = new ComedyWindow();
                comedyPage.GenerateStory(nouns, verbs);                
                comedyPage.Show();
               
            }

            else if (selector.SelectedValue == horror)
            {
                Console.WriteLine("Horror Selected");
                HorrorWindow horrorPage = new HorrorWindow();
                horrorPage.GenerateStory(nouns, verbs);
                horrorPage.Show();
            }
            
            

        }
    }
}
