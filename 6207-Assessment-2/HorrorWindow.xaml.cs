﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _6207_Assessment_2
{
    /// <summary>
    /// Interaction logic for HorrorWindow.xaml
    /// </summary>
    public partial class HorrorWindow : Window
    {
        public HorrorWindow()
        {
            InitializeComponent();
        }

        public void GenerateStory(string[] nouns, string[] verbs)
        {

            string story = @"A young " + nouns[6] + " named Lisa often had to spend time alone at home at night, as her parents " + verbs[5] + "ed late. " +
                        System.Environment.NewLine+
                        System.Environment.NewLine +
                        "They bought her a " + nouns[5]+" to keep her company and "+verbs[3] +" her. "+
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "One night Lisa was awakened by a " + verbs[6] + "ing sound. She got up and "+verbs[1] +"ed to the "+nouns[8] +" to make sure the tap was off. " +
                        System.Environment.NewLine +
                        "As she was getting back into the bed she stuck her hand under the bed, and the " + nouns[5] + " " + verbs[2] + "ed" + " it. " +
                        System.Environment.NewLine +
                        System.Environment.NewLine +
                        "The " + verbs[6] + "ing sound continued, so she went to the " + nouns[7] + " and made sure the tap was turned off there, too. " +
                        System.Environment.NewLine +
                        "She went back to her " + nouns[2] + " and stuck her hand under the " + nouns[4] + ", and the " + nouns[5] + " " + verbs[2] + "ed"+" it again. " +
                        System.Environment.NewLine +
                        System.Environment.NewLine +

                        "But the " + verbs[6] + "ing continued, so she "+verbs[7] +" outside and turned off all the "+nouns[7] +" out there. " +
                        System.Environment.NewLine +
                        System.Environment.NewLine +

                        "She came back to " + nouns[4] + ", " + verbs[8]+ "ed her hand under it, and the " + nouns[5] + " " + verbs[2]+ "ed it again. "+
                        System.Environment.NewLine +
                        System.Environment.NewLine +

                        "The " + verbs[6]+"ing continued: "+verbs[6]+", "+verbs[6] + ", " + verbs[6] +"."+
                        System.Environment.NewLine +
                        System.Environment.NewLine +

                        "This time she " + verbs[4]+ " and located the source of the " + verbs[6] + "ing, it was coming from her "+nouns[1] +"!" +
                        System.Environment.NewLine +
                        "She opened the " + nouns[1] + " door, and there found her poor " + nouns[5] + " hanging upside down with its "+ nouns[3]+" cut. " +
                        System.Environment.NewLine +
                        System.Environment.NewLine +

                        "Written on the inside of the " + nouns[1] + " was, 'Humans can " + verbs[2]+", too!'";

            

            foreach (string n in nouns)
            {
                int i = 1;
                Console.WriteLine("Noun: " + nouns[i]);
                i++;
            }
            Console.WriteLine();
            foreach (string v in verbs)
            {
                int i = 1;
                Console.WriteLine("Verb: " + verbs[i]);
                i++;
            }

            horrorStory.Text = story;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Pdf pdf = new Pdf();
            Uri path = new Uri(AppDomain.CurrentDomain.BaseDirectory +"file.xps", UriKind.Absolute);
            string pdfPath = AppDomain.CurrentDomain.BaseDirectory + "file.pdf";
            string xpsPath = AppDomain.CurrentDomain.BaseDirectory + "file.xps";

            pdf.convertToXps(path, wholeGrid);
            pdf.convertToPdf(xpsPath,pdfPath);
        }
    }
}
