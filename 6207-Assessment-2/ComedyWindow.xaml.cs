﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _6207_Assessment_2
{
    /// <summary>
    /// Interaction logic for ComedyWindow.xaml
    /// </summary>
    public partial class ComedyWindow : Window
    {
        public ComedyWindow()
        {
            InitializeComponent();
        }

        public void GenerateStory(string[] nouns, string[] verbs)
        {

            string story = @"A " + nouns[6] + " is " + verbs[5] + "ing around town and he sees a sign in front of a " + nouns[8] + ":" + Environment.NewLine +
                "Talking "+ nouns[3]+" For Sale." + Environment.NewLine +
                Environment.NewLine +
                "He rings the bell and the " + nouns[1] + " tells him the " + nouns[3] + " is in the backyard. The " + nouns[6] + " " + verbs[3] +
                Environment.NewLine + "s into the backyard and sees a " + nouns[3] + " " + verbs[2] + "ing there." + Environment.NewLine +
                Environment.NewLine +
                @"'You talk?' he asks. 'Yep,' the " + nouns[3] + " replies." + Environment.NewLine +
                Environment.NewLine +
                "'So, what's your story?' the " + nouns[6] + " asked." + Environment.NewLine +
                Environment.NewLine +
                "The " + nouns[3] + " looks up and says, 'Well, I discovered that I could " + verbs[6] +
                Environment.NewLine + " when I was pretty young. I wanted to help the government, " + Environment.NewLine +
                "so I told them about my gift, and in no time at all they had me " + verbs[4] +
                Environment.NewLine + "ing from country to country, sitting in rooms with " + nouns[2] + "s and " + nouns[7] + ", because" + Environment.NewLine +
                " no one figured a " + nouns[3] + " would be " + verbs[8] + "ing. I was one of their most valuable " + nouns[2] +
                Environment.NewLine + "s for eight years running. But the " + verbs[4] + "ing around really tired me out," + Environment.NewLine +
                " and I knew I wasn't getting any younger so I decided to " + verbs[1] + ". I signed up for a job at the " + nouns[5] +
                Environment.NewLine + " to do some undercover security wandering near " + Environment.NewLine +
                "suspicious characters and " + verbs[7] + " in. I uncovered some incredible dealings and was awarded a batch of medals. " + Environment.NewLine +
                "I got married, had a baby " + nouns[3] + ", and now I'm just retired.'" + Environment.NewLine +
                Environment.NewLine +
                "The " + nouns[6] + " is amazed. He " + verbs[3] + "s back in the " + nouns[8] + " and asks the " + nouns[1] + " what he wants for the " + nouns[3] + "." + Environment.NewLine +
                Environment.NewLine +
                "'" + nouns[7] + ",' the " + nouns[1] + " says.'" + Environment.NewLine +
                Environment.NewLine +
                "'" + nouns[7] + "? This " + nouns[3] + " is amazing. Why on earth are you selling him so cheap?'" + Environment.NewLine +
                Environment.NewLine +
                "'Because he's a liar. He never did any of that stuff.'" ;


            foreach (string n in nouns)
            {
                int i = 1;
                Console.WriteLine("Noun: " + nouns[i]);
                i++;
            }
            Console.WriteLine();
            foreach (string v in verbs)
            {
                int i = 1;
                Console.WriteLine("Verb: " + verbs[i]);
                i++;
            }

            comedyStory.Text = story;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Pdf pdf = new Pdf();
            Uri path = new Uri(AppDomain.CurrentDomain.BaseDirectory + "file.xps", UriKind.Absolute);
            string pdfPath = AppDomain.CurrentDomain.BaseDirectory + "file.pdf";
            string xpsPath = AppDomain.CurrentDomain.BaseDirectory + "file.xps";

            pdf.convertToXps(path, wholeGrid);
            pdf.convertToPdf(xpsPath, pdfPath);
        }
    }
}
